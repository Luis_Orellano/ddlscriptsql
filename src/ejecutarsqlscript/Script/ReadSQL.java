/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejecutarsqlscript.Script;

import ejecutarsqlscript.ConexionDB.ConectaDB;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.*;

/**
 * Clase encargada de ejectutar un archivo .sql
 *
 * @author Luis
 */
public class ReadSQL {

    /**
     * metodo que ejecuta las query
     *
     * @param query String que representa la query
     * @return true si la query se ejecuto correctamente, false si no
     */
    private boolean executeQuery(String[] query, String dataBase) {
        Statement sentencias = null;
        ConectaDB connector = new ConectaDB(dataBase);

        Connection con = connector.realizarConexion();

        if (connector.hasError()) {
            return false;
        }

        try {
            sentencias = con.createStatement();
            for(int i = 0; i < query.length; i++){
                sentencias.executeUpdate(query[i]);
                //System.out.println(query[i]);
            }
            sentencias.close();
            con.close();
        } catch (SQLException e) {
            return false;
        }
        return true;
    }

    /**
     * metodo para transformar el documento a una cadena de caracteres
     *
     * @param document file del documento
     * @return retorno, cadena de caracteres del file
     */
    private String readContent(File document) {
        String sCadena = "";
        String retorno = "";

        if (!document.exists()) {
            return null;
        }

        try {
            BufferedReader bf = new BufferedReader(new FileReader(document));
            while ((sCadena = bf.readLine()) != null) {
                retorno += sCadena;
            }
        } catch (FileNotFoundException fnfe) {
            return null;
        } catch (IOException ioe) {
            return null;
        }

        //System.out.println(retorno);

        return retorno;
    }

    private String[] separedString(String cadena, String separator) {
        //System.out.println("Separator: " + separator);
        String[] parts = null;
        if (separator.equals("|") || separator.equals("\\") || separator.equals(".") || separator.equals("^") || separator.equals("$")
                || separator.equals("?") || separator.equals("*") || separator.equals("+") || separator.equals("(") || separator.equals(")")
                || separator.equals("{") || separator.equals("[")) {
            //Es metacaracter!
            parts = cadena.split("\\" + separator);
        } else {
            //No es metacaracter.
            parts = cadena.split(separator);
        }
        return parts;

    }

    /**
     * metodo que lee el documento, lo guarda en un String y ejecuta las querys
     * del archivo.
     * @param documento file a ejecutar, recibe el directorio del documento.
     * @param dataBase nombre de la base de datos a crear.
     * @return las consultas
     */
    public boolean importarQuery(File documento, String dataBase) {
        String query = readContent(documento);
        String[] querySeparado = separedString(query, ";");
        return executeQuery(querySeparado, dataBase);
    }

}
