/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejecutarsqlscript.ConexionDB;

import java.sql.*;
/**
 * Clase encargada de conectar establecer la conexion con una base de datos
 * @author Luis
 */
public class ConectaDB {
    private String dbDriver = "org.sqlite.JDBC";
    private String dbName;
    private String mensajeError = "";

    /**
     * Constructor para crear la base de datos
     * @param dbName ruta con el nombre de la base de datos
     */
    public ConectaDB (String dbName){
        this.dbName = dbName;
    }
    
    /**
     * metodo para realizar la conexion
     * @return conector
     */
    public Connection realizarConexion(){
        Connection conectar = null;
        
        try {
            Class.forName(dbDriver).newInstance();
        } catch (ClassNotFoundException cnfe) {
            return null;
        } catch (InstantiationException  ie) {
            return null;
        } catch (IllegalAccessException iae) {
            return null;
        }
        
        try {
            conectar = DriverManager.getConnection("jdbc:sqlite:"+this.dbName);
        } catch (SQLException ex) {
            return null;
        }
        
        return conectar;
    }
    
    public boolean hasError() {
        return this.mensajeError.length() > 0;
    }
    
    public String getDbName() {
        return dbName;
    }

    public void setDbName(String dbName) {
        this.dbName = dbName;
    }
    
    public String getError(){
        return this.mensajeError;
    }
    
    
}
