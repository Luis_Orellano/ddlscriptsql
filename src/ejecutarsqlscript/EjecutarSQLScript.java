/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejecutarsqlscript;

import ejecutarsqlscript.Script.ReadSQL;
import java.io.File;

/**
 *
 * @author Luis
 */
public class EjecutarSQLScript {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //Instancia de la clase encargada de leer el .SQL
        ReadSQL rsql = new ReadSQL();
        
        //Referencia al archivo .SQL
        File file = new File("Ejemplo.sql");
        
        //Metodo para ejecutar el archivo, y el nombre de la base de datos
        rsql.importarQuery(file, "Prueba");
    }
    
}
