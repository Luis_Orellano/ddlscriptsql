##ddlscriptsql

#Libreria para ejecutar consultas DDL (create, alter, drop) a partir de un archivo .SQL

#Ejemplo de utilizacion:

#//Instancia de la clase encargada de leer el .SQL
    
#    ReadSQL rsql = new ReadSQL();
        
#//Referencia al archivo .SQL
    
#    File file = new File("Ejemplo.sql");
        
#//Metodo para ejecutar el archivo, y el nombre de la base de datos
    
#    rsql.importarQuery(file, "Prueba");


#PD: El archivo .SQL no tiene que contener ningun tipo de comentario en el codigo
